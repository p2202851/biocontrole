#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "imgui-master/imgui.h"
#include "imgui-sfml-2.6.x/imgui-SFML.h"

int main() {
    Menu menu;
    bool afficheBoutiqueP = false;
    sf::VideoMode mode(800, 400);
    if (!mode.isValid()) {
        mode = sf::VideoMode::getDesktopMode();
    }
    menu.window.create(mode, "My window", sf::Style::Fullscreen);
    menu.window.setFramerateLimit(60);
    if(!ImGui::SFML::Init(menu.window)){
        return -1;
    }
    sf::Clock deltaClock;
    while (menu.window.isOpen()) {
        sf::Event event;
        while (menu.window.pollEvent(event)) {
            ImGui::SFML::ProcessEvent(menu.window, event);
            switch (event.type) {
                case sf::Event::Closed:
                    menu.window.close();
                }
                break;

                case sf::Event::MouseButtonPressed:
                    if (event.mouseButton.button == sf::Mouse::Left)
                {
                    if (menu.spriteBoutonDeb.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                    {
                        //menu.start();
                    }
                    if (menu.spriteBoutonGal.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                    {
                        //menu.afficherGal();
                    }
                    if (menu.spriteBoutonOpt.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                    {
                        //menu.afficherOption();
                    }
                    if (menu.spriteBoutonBoutique.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                    {
                        afficheBoutiqueP = !afficheBoutiqueP;
                    }
                }
            default:
                break;
                    break;

                case sf::Event::KeyPressed:
                    if (event.key.code == sf::Keyboard::Escape) {
                        menu.window.close();
                    }
                    break;
                default:
                    break;
            }
        }
        ImGui::SFML::Update(menu.window, deltaClock.restart());
        if (afficheBoutiqueP)
        {
            menu.afficheBoutiqueP();
        } else {
            menu.afficheDeck();
        }
        menu.afficheBoutiqueC();
        menu.afficheInfoCase(0, 0);
        menu.window.clear();
        menu.afficherMenu();
        ImGui::SFML::Render(menu.window);
        menu.window.display();
    }
    ImGui::SFML::Shutdown();
    return 0;
}