#include "Ravageur.h"
#include <iostream>

using namespace std;

int main() {
    Ravageur r(5);
    unsigned int id;
    int nb_cible, nb_menance, pourcentage;
    string image, nom;
    vector<int> cible;
    bool propage;
    id = r.getid();
    nb_cible = r.getnb_cible();
    nb_menance = r.getnb_menace();
    image = r.getim();
    pourcentage = r.getpourcentage();
    cout << "id: " << id << endl;
    cout << "nb_cible: " << nb_cible << endl;
    cout << "nb_menance: " << nb_menance << endl;
    for (int i = 0; i < nb_cible; i++) {
        cout << "cible: " << r.getcible(i) << endl;
    }
    for (int i = 0; i < nb_menance; i++) {
        cout << "menace: " << r.getmenace(i) << endl;
    }

    cout << "image: " << image << endl;
    cout << "pourcentage: " << pourcentage << endl;
    return 0;
}