#include "Plateau.h"

Plateau::Plateau() {
    for (double i = 0; i < longueur * 20; i += 20) {
        for (double j = 0; j < largeur * 10; j += 10) {
            Vect2D v(i, j);
            Case c(v);
            tabCase[(int) i / 20][(int) j / 10] = c;
        }
    }
}

Plateau::~Plateau() {
}

Case Plateau::getTabCaseVect(Vect2D vect) const {
    for (int i = 0; i < longueur; i++) {
        for (int j = 0; j < largeur; j++) {
            if (vect == tabCase[i][j].getVect2D()) {
                return tabCase[i][j];
            }
        }
    }
    Case c;
    return c;
}

Case Plateau::getTabCaseIndice(int i, int j) const {
    return tabCase[i][j];
}

void Plateau::setTabCase(Case c) {
    Vect2D vect = c.getVect2D();
    for (int i = 0; i < longueur; i++) {
        for (int j = 0; j < largeur; j++) {
            if (vect == tabCase[i][j].getVect2D()) {
                tabCase[i][j] = c;
            }
        }
    }
}

int Plateau::calculeValeur() {
    int val = 0;
    for (int i = 0; i < longueur; i++) {
        for (int j = 0; j < largeur; j++) {

            val += getTabCaseIndice(i, j).calculeValeur();
        }
    }
    return val;
}

void Plateau::affichePlateau() const {
}
