#include "Jeu.h"
#include <iostream>
#include <random>
#include "Ravageur.h"
#include "Plateau.h"

using namespace std;

Jeu::Jeu() : round(1), argent(300) {
    for (int i = 1; i <= 8; i++) {
        Protecteur p(i);
        shopProtect[i - 1] = p;
    }
    for (int j = 1; j <= 5; j++) {
        Culture c(j);
        shopCulture[j - 1] = c;
    }
    Protecteur p(0);
    for (int k = 0; k < 10; k++) {
        decjJoueur[k] = p;
    }
}

Jeu::~Jeu() {
}

void Jeu::afficheJeu() const {
    cout << "Round : " << round << endl;
    cout << "Argent : " << argent << endl;
    cout << "Shop Protecteurs : " << endl;
    for (int i = 0; i < 8; i++) {
        cout << shopProtect[i].getid() << " : " << shopProtect[i].getprix() << " | ";
    }
    cout << endl;
    cout << "Shop Cultures : " << endl;
    for (int i = 0; i < 5; i++) {
        cout << shopCulture[i].getidcult() << " : " << shopCulture[i].getprix() << " | ";
    }
    cout << endl;
    cout << "Deck joueur : " << endl;
    for (int i = 0; i < 10; i++) {
        cout << decjJoueur[i].getid() << " ";
    }
    cout << endl;
}

void Jeu::utiliserProtecteur(int i, int x, int y) {
    Protecteur p = retireDeckjJoueur(i);
    if (p.getid() == 0) {
        cout << "Ce protecteur n'est pas dans votre deck" << endl;
    } else {
        Vect2D vect(x * 20, y * 10);
        Case c(vect);
        c = plateau.getTabCaseVect(vect);
        c.ajouteProtecteur(p.getid());
        plateau.setTabCase(c);
    }
}

void Jeu::getShopProtect(Protecteur shop[13]) const {
    for (int i = 0; i < 13; i++) {
        shop[i] = shopProtect[i];
    }
}

void Jeu::getShopCulture(Culture shop[5]) const {
    for (int i = 0; i < 5; i++) {
        shop[i] = shopCulture[i];
    }
}

Plateau Jeu::getPlateau() const {
    return plateau;
}

unsigned int Jeu::getround() const {
    return round;
}

int Jeu::getargent() const {
    return argent;
}

void Jeu::plusRound() {
    round++;
}

void Jeu::modifArgent(unsigned int a) {
    argent += a;
}

void Jeu::ajoutDeckjJoueur(Protecteur p) {
    for (int i = 0; i < 10; i++) {
        if (decjJoueur[i].getid() == 0) {
            decjJoueur[i] = p;
            break;
        }
    }
}

Protecteur Jeu::retireDeckjJoueur(int p) {
    Protecteur protec(0);
    bool trouve = false;
    for (int i = 0; i < 10; i++) {
        if (decjJoueur[i].getid() == p) {
            trouve = true;
            protec = decjJoueur[i];
        }
        if (trouve && i < 9) {
            decjJoueur[i] = decjJoueur[i + 1];
        }
    }
    if (trouve) {
        decjJoueur[9] = Protecteur(0);
    } else {
        cout << "Ce protecteur n'est pas dans votre deck" << endl;
    }
    return protec;
}

void Jeu::achatProtecteur(int p) {
    if (argent >= shopProtect[p - 1].getprix()) {
        modifArgent(-shopProtect[p - 1].getprix());
        ajoutDeckjJoueur(shopProtect[p - 1]);
    } else {
        cout << "Vous n'avez pas assez d'argent" << endl;
    }
}

void Jeu::achatCulture(int cult, int x, int y) {
    Vect2D vect(x * 20, y * 10);
    int prix = shopCulture[cult - 1].getprix();
    if (plateau.getTabCaseVect(vect).getcult().getidcult() != 0) {
        cout << "Il y a déjà une culture sur cette case" << endl;
    } else {
        if (argent >= prix) {
            modifArgent(-prix);
            Case c(vect);
            c.rensCult(cult);
            plateau.setTabCase(c);
        } else {
            cout << "Vous n'avez pas assez d'argent" << endl;
        }
    }
}

void Jeu::finround() {
    srand(time(NULL));
    plusRound();
    modifArgent(plateau.calculeValeur());
    cout << "Vous avez gagné " << plateau.calculeValeur() << " pièces" << endl;
    int const id = rand() % 8 + 1;
    Ravageur r(id);
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 5; j++) {
            if (r.cibleEstPresente(plateau.getTabCaseIndice(i, j)) &&
                !r.protectPresent(plateau.getTabCaseIndice(i, j))) {
                Vect2D v(i * 20, j * 10);
                Case c(v);
                c = plateau.getTabCaseVect(v);
                c.ajouteRavageur(r.getid());
                plateau.setTabCase(c);
            }
        }
    }
}
