#include "Info.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Info::Info() {
    id = 0;
    image = "";
    description = "";
    nom = "";
    type = "";
}

void Info::InfoCult(int idCult) {
    ifstream detail("../data/Culture.txt");
    if (detail.is_open()) {
        string tampon;
        detail >> id;
        while (idCult != id) {
            getline(detail, tampon);
            detail >> id;
        }
        detail >> nom;
        detail >> image;
        detail >> description;
    } else {
        cout << "Erreur à l'ouverture" << endl;
    }
    detail.close();
}

void Info::InfoRav(int idRav) {
    ifstream detail("../data/Ravageurs.txt");
    if (detail.is_open()) {
        string tampon;
        detail >> id;
        while (idRav != id) {
            getline(detail, tampon);
            detail >> id;
        }
        detail >> nom;
        detail >> image;
        detail >> description;
    } else {
        cout << "Erreur à l'ouverture" << endl;
    }
    detail.close();
}

void Info::InfoProt(int idProt) {
    ifstream detail("../data/Protecteur.txt");
    if (detail.is_open()) {
        string tampon;
        detail >> id;
        while (idProt != id) {
            getline(detail, tampon);
            detail >> id;
        }
        detail >> nom;
        detail >> image;
        detail >> description;
    } else {
        cout << "Erreur à l'ouverture" << endl;
    }
    detail.close();
}

Info::~Info() {
}

unsigned int Info::getid() const {
    return id;
}

string Info::getimage() const {
    return image;
}

string Info::getdescription() const {
    return description;
}

string Info::getnom() const {
    return nom;
}

string Info::gettype() const {
    return type;
}