#include "Culture.h"
#include <string>
#include <iostream>

using namespace std;

int main() {
    unsigned int points, id, prix;
    string nom, im, type;
    Culture c;
    id = c.getidcult();
    nom = c.getnom();
    prix = c.getprix();
    type = c.gettype();
    im = c.getim();
    points = c.getnbpoint();
    cout << "id: " << id << endl;
    cout << "nom: " << nom << endl;
    cout << "prix: " << prix << endl;
    cout << "type: " << type << endl;
    cout << "im: " << im << endl;
    cout << "points: " << points << endl;
    c.~Culture();
    Culture c1(5);
    id = c1.getidcult();
    nom = c1.getnom();
    prix = c1.getprix();
    type = c1.gettype();
    im = c1.getim();
    points = c1.getnbpoint();
    cout << "id: " << id << endl;
    cout << "nom: " << nom << endl;
    cout << "prix: " << prix << endl;
    cout << "type: " << type << endl;
    cout << "im: " << im << endl;
    cout << "points: " << points << endl;
    c1.~Culture();
}