#include "Galerie.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Galerie::Galerie() {
    type = "";
}

Galerie::~Galerie() {
}

void Galerie::afficherGalerie() const {
    ifstream galerie;
    unsigned int id;
    galerie.open("../data/" + type + ".txt");
    if (galerie.is_open()) {
        string tampon;
        galerie >> id;
        while (id != 0) {
            getline(galerie, tampon);
            galerie >> id;
            cout << tampon;
        }
    } else {
        cout << "Impossible d'ouvrir le fichier" << endl;
    }

}