#include "Plateau.h"
#include "Protecteur.h"
#include <iostream>


using namespace std;

int main() {
    Vect2D vect(0, 0);
    Vect2D vect2(20, 20);

    unsigned int valeur, val2;
    vector<int> tabProtect, tabProtect2;
    vector<int> tabRavageur, tabRavageur2;
    Case c(vect);
    Case c2(vect2);
    vect = c.getVect2D();
    valeur = c.getvaleur();
    tabRavageur = c.gettabRavageur();
    cout << "vect : " << vect << endl << "val : " << valeur << endl;
    cout << "champs : " << c.getcult().getidcult() << endl;

    cout << "le tab de rav" << endl;
    for (int i = 0; i < tabRavageur.size(); i++) {
        // cout<<tabRavageur[i].getid()<<endl;
    }
    c.ajouteProtecteur(5);
    tabProtect = c.gettabProtect();
    cout << "ajout du prot 1" << endl;
    for (int i = 0; i < tabProtect.size(); i++) {
        cout << "la longueur est: " << tabProtect.size() << endl;
        cout << tabProtect[i] << endl;
    }
    c.rensCult(3);
    valeur = c.calculeValeur();
    cout << "la val de la case : " << valeur << endl;
    cout << endl << "le tab de protect 2 :" << endl;
    c2.ajouteProtecteur(5);
    c2.ajouteProtecteur(3);
    c2.ajouteProtecteur(2);
    c2.ajouteProtecteur(1);
    c2.rensCult(1);
    tabProtect = c2.gettabProtect();
    for (int i = 0; i < tabProtect.size(); i++) {
        Protecteur prot(tabProtect[i]);
        cout << "l'ID du prot est : " << prot.getid() << endl;
    }
    cout << "le nom du champs est : " << c2.getcult().getnom() << endl;

    Plateau p;
    cout << "set case 1" << endl;
    p.setTabCase(c);
    cout << "set ok" << endl;
    Case ca;
    ca = p.getTabCaseVect(c.getVect2D());
    vector<int> tab = ca.gettabProtect();
    cout << "tab rempli" << endl;
    cout << tab.size() << endl;
    for (int i = 0; i < tab.size(); i++) {
        Protecteur prot(tab[i]);
        cout << "l'ID du prot est : " << prot.getid() << endl;
    }
    p.setTabCase(c2);
    vector<int> tab2;
    tab2 = p.getTabCaseVect(c2.getVect2D()).gettabProtect();
    for (int i = 0; i < tab2.size(); i++) {
        Protecteur prot(tab2[i]);
        cout << "l'ID du prot est : " << prot.getid() << endl;
    }
    cout << "le plateau vaut : " << p.calculeValeur() << endl;

    return 0;
}