#include "Jeu.h"
#include <iostream>
#include "imgui-sfml-2.6.x/imgui-SFML.h"
#include <SFML/Graphics.hpp>
#include "imgui-master/imgui.h"

using namespace std;

int main() {
    Jeu j;
    j.afficheJeu();

    // Teste de la fonction getprotect
    Protecteur shop[13];
    j.getShopProtect(shop);
    cout << "======== GetShop protecteurs ========" << endl;
    for (int i = 0; i < 13; i++) {
        cout << shop[i].getid() << " ";
    }
    cout << endl;

    // Teste de la fonction getculture
    cout << "======== GetShop cultures ========" << endl;
    Culture shopC[5];
    j.getShopCulture(shopC);
    for (int i = 0; i < 5; i++) {
        cout << shopC[i].getidcult() << " ";
    }
    cout << endl;

    // Teste de la fonction getRound et getArgent
    int argent = j.getargent();
    cout << "Argent : " << argent << endl;
    unsigned int round = j.getround();
    cout << "Round : " << round << endl;

    cout << "======== nouveau round ========" << endl;
    // Teste de la fonction plusRound et modifArgent
    j.plusRound();
    j.modifArgent(25);

    // Teste de la fonction ajoutDeckjJoueur
    Protecteur p(5);
    Protecteur p2(6);
    Protecteur p3(7);
    j.ajoutDeckjJoueur(p);
    j.ajoutDeckjJoueur(p2);
    j.ajoutDeckjJoueur(p3);
    j.afficheJeu();

    // Teste de la fonction retireDeckjJoueur
    j.retireDeckjJoueur(1);
    j.retireDeckjJoueur(6);
    j.afficheJeu();

    // Teste de la fonction achatProtecteur et culture
    cout << "======== achat protecteur et culture ========" << endl;
    j.achatProtecteur(3);
    j.achatCulture(2, 1, 1);
    j.afficheJeu();
    Plateau plat;
    plat = j.getPlateau();
    cout << "======== affiche case ========" << endl;
    plat.getTabCaseIndice(1, 1).afficheCase();

    j.achatCulture(2, 1, 1);
    j.achatCulture(5, 2, 3);
    j.achatProtecteur(1);

    // Teste de la fonction utiliserProtecteur
    cout << "======== utiliser protecteur ========" << endl;
    j.utiliserProtecteur(1, 1, 1);
    j.utiliserProtecteur(5, 2, 3);
    j.afficheJeu();
    plat = j.getPlateau();
    plat.getTabCaseIndice(2, 3).afficheCase();

    // Teste de la fonction finround
    cout << "======== fin round ========" << endl;
    j.finround();
    j.afficheJeu();
    cout << "======== affiche case ========" << endl;
    plat = j.getPlateau();
    plat.getTabCaseIndice(1, 1).afficheCase();

    return 0;
}