#ifndef PROTECTEUR
#define PROTECTEUR

#include "Case.h"
#include <vector>
#include <string>

using namespace std;

class Case;

class Protecteur {

private:
    unsigned int id, de, efficacite;
    int nb_cible, prix;
    string image, nom;
    vector<int> cible;
    bool propage, reste;

public:
    Protecteur() {};

    Protecteur(int id_nouv);

    /*Post-condition le protecteur est initialisé avec les valeurs presentes dans le fichier*/

    ~Protecteur();

    /*Post-condition le protecteur est détruit*/

    int getid() const;

    /*Post-condition retourne l'id du protecteur*/

    int getde() const;

    /*Post-condition retourne le lancé de dé demandé par le protecteur*/

    int getnb_cible() const;

    /*Post-condition retourne le nombre de cible du protecteur*/

    int getprix() const;

    /*Post-condition retourne le prix du protecteur*/

    bool getreste() const;

    /*Post-condition retourne vrai si le protecteur reste sur la case et faux sinon*/

    string getnom() const;

    /*Post-condition retourne le nom du protecteur*/

    int getcible(int i) const;

    /*Pré-condition i est compris entre 0 et nb_cible
      Post-condition retourne la cible i du protecteur*/

    int getefficacite() const;

    /*Post-condition retourne l'efficacité du protecteur*/

    string getim() const;

    /*Post-condition retourne l'image du protecteur*/

    bool cibleEstPresente(const Case &c) const;

    /*Pré-condition la protecteur se trouve sur la case c
      Post-condition retourne vrai si la cible est présente sur la case et faux sinon*/

    void fonctionne(Case c);

    /*Pré-condition la protecteur se trouve sur la case c
      Post-condition le protecteur agit sur la case c*/

    void afficheProtecteur() const;
    /*Post-condition affiche le protecteur*/
};

#endif