
#ifndef VECT2D_H
#define VECT2D_H

#include <iostream>

using namespace std;

class Vect2D {
private:
    double x;
    double y;

public:
    // Constructeur par défaut
    Vect2D();

    // Constructeur avec des valeurs spécifiques
    Vect2D(double x_val, double y_val);

    // Opérateur d'addition surchargé
    Vect2D operator+(const Vect2D &other) const;

    // Opérateur de soustraction surchargé
    Vect2D operator-(const Vect2D &other) const;

    // Opérateur de multiplication par un scalaire surchargé
    Vect2D operator*(double scalar) const;

    // Opérateur de division par un scalaire surchargé
    Vect2D operator/(double scalar) const;

    // Opérateur d'égalité surchargé
    bool operator==(const Vect2D &other) const;

    [[nodiscard]] double getX() const;

    // Getter pour la composante y
    [[nodiscard]] double getY() const;

    // Setter pour la composante x
    void setX(double x_val);

    // Setter pour la composante y
    void setY(double y_val);

    // Affichage surchargé
    friend ostream &operator<<(ostream &os, const Vect2D &vecteur);
};

#endif // VECT2D_H
