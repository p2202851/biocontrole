#include "Case.h"
#include "Ravageur.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

Ravageur::Ravageur(int id_nouv) {
    ifstream Info("../data/Ravageur.txt");
    if (Info.is_open()) {
        string tampon;
        Info >> id;
        while (id_nouv != id) {
            getline(Info, tampon);
            Info >> id;
        }
        Info >> nom;
        Info >> image;
        Info >> nb_cible;
        for (int i = 0; i < nb_cible; i++) {
            int tampon;
            Info >> tampon;
            cible.push_back(tampon);
        }
        Info >> nb_menace;
        for (int i = 0; i < nb_menace; i++) {
            int tampon;
            Info >> tampon;
            menace.push_back(tampon);
        }
        Info >> pourcentage;
        Info >> tampon;
        if (tampon == "true") {
            propage = true;
        } else {
            propage = false;
        }
    } else {
        cout << "Erreur à l'ouverture" << endl;
    }
    Info.close();
}

Ravageur::~Ravageur() {
}

int Ravageur::getid() const {
    return id;
}

string Ravageur::getim() const {
    return image;
}

int Ravageur::getnb_cible() const {
    return nb_cible;
}

int Ravageur::getpourcentage() const {
    return pourcentage;
}

int Ravageur::getcible(int i) const {
    return cible[i];
}

int Ravageur::getnb_menace() const {
    return nb_menace;
}

int Ravageur::getmenace(int i) const {
    return menace[i];
}

void Ravageur::afficheRavageur() const {
    cout << "Ravageur " << nom << " : " << endl;
    for (int i = 0; i < nb_cible; i++) {
        cout << "Cible " << i + 1 << " : " << cible[i] << endl;
    }
    cout << endl;
    for (int i = 0; i < nb_menace; i++) {
        cout << "Menace " << i + 1 << ": " << menace[i] << endl;
    }
    cout << endl;
    cout << "Pourcentage : " << pourcentage << endl;
    cout << "Propage : " << propage << endl;
}

bool Ravageur::cibleEstPresente(const Case &c) const {
    int i;
    int culture = c.getcult().getidcult();
    for (i = 0; i < cible.size(); i++) {
        if (cible[i] == culture) {
            return true;
        }
    }
    return false;
}

bool Ravageur::protectPresent(const Case &c) const {
    vector<int> tabProtect = c.gettabRavageur();
    for (int i = 0; i < tabProtect.size(); i++) {
        for (int j = 0; j < menace.size(); j++) {
            if (tabProtect[i] == menace[j]) {
                return true;
            }
        }
    }
    return false;
}