#include "affichagetxt.h"
#include "Jeu.h"

#include <iostream>
#include <string>

int main() {
    AffichageTxt a;
    a.afficherInfoJeu();
    a.afficherPlateau();
    a.afficherDetails(0, 0);
    a.afficheInfoProtecteur(1);
    return 0;
}