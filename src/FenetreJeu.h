#ifndef FenetreJeu_H
#define FenetreJeu_H

#include "Jeu.h"
#include <SFML/Graphics.hpp>
#include "imgui-master/imgui.h"
#include "imgui-sfml-2.6.x/imgui-SFML.h"

class FenetreJeu {
private:
    int longueur;
    int largeur;
    bool jeuSet, boutiquePSet, boutiqueCSet;

public:
    sf::RenderWindow window;

    FenetreJeu();

    ~FenetreJeu();

    void setFenetreJeu(); // Initialise la fenêtre de jeu

    bool getJeuSet(); // Retourne la valeur de jeuSet

    bool getBoutiquePSet(); // Retourne la valeur de boutiquePSet

    bool getBoutiqueCSet(); // Retourne la valeur de boutiqueCSet

    void setBoutiqueP(); // Initialise la boutique de protecteur

    void setBoutiqueC(); // Initialise la boutique de culture

    void start(); // Lance le jeu

    void ouvreBoutiqueP(); // Ouvre la boutique de protecteur

    void ouvreBoutiqueC(); // Ouvre la boutique de culture
};

#endif