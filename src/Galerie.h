#ifndef GALERIE_H
#define GALERIE_H
using namespace std;

#include <string>
#include "Info.h"
#include <vector>

class Galerie {
private:
    string type;
    vector<Info> galerie;
public:
    Galerie(); // Constructeur de la classe
    /*Post-condition la galerie est initialisé avec les valeurs passée en parametres*/

    ~Galerie()= default; // Destructeur de la classe
    /*Post-condition la galerie est détruite*/

    void afficherGalerie() const; // Affiche la galerie

};

#endif
