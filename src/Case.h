#ifndef CASE
#define CASE

#include "Culture.h"
#include "Vect2D.h"
#include <vector>

using namespace std;

class Case {
private:
    Vect2D vect;
    Culture champs;
    int valeur;
    vector<int> tabProtect;
    vector<int> tabRavageur;

public:
    Case();

<<<<<<< HEAD
  explicit Case(Vect2D const &v);
  /*Post-condition la case est initialisé avec les valeurs passées en parametres*/
||||||| parent of 461ce8c (reformat)
  Case(Vect2D v);
  /*Post-condition la case est initialisé avec les valeurs passées en parametres*/
=======
    Case(Vect2D v);
>>>>>>> 461ce8c (reformat)

    /*Post-condition la case est initialisé avec les valeurs passées en parametres*/

<<<<<<< HEAD
  [[nodiscard]] Vect2D getVect2D() const;
  /*Post-condition retourne la coordonnée x de la case*/
||||||| parent of 461ce8c (reformat)
  Vect2D getVect2D() const;
  /*Post-condition retourne la coordonnée x de la case*/
=======
    ~Case();
>>>>>>> 461ce8c (reformat)

    /*Post-condition la case est détruite*/

<<<<<<< HEAD
  void setVect2D(Vect2D const &v);
  /*Post-condition la culture de la case est modifiée*/
||||||| parent of 461ce8c (reformat)
  void setVect2D(Vect2D v);
  /*Post-condition la culture de la case est modifiée*/
=======
    Vect2D getVect2D() const;
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  [[nodiscard]] Culture getcult() const;
  /*Post-condition retourne la culture de la case*/
||||||| parent of 461ce8c (reformat)
  Culture getcult() const;
  /*Post-condition retourne la culture de la case*/
=======
    /*Post-condition retourne la coordonnée x de la case*/
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  void setcult(Culture const &c);
  /*Post-condition la culture de la case est modifiée*/
||||||| parent of 461ce8c (reformat)
  void setcult(Culture c);
  /*Post-condition la culture de la case est modifiée*/
=======
    void rensCult(int id);
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  [[nodiscard]] int getvaleur() const;
  /*Pré-condition un champs est présent sur la case
    Post-condition retourne la valeur de la case*/
||||||| parent of 461ce8c (reformat)
  int getvaleur() const;
  /*Pré-condition un champs est présent sur la case
    Post-condition retourne la valeur de la case*/
=======
    void setVect2D(Vect2D v);
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  void setvaleur(int val);
||||||| parent of 461ce8c (reformat)
  void setvaleur(unsigned int val);
=======
    /*Post-condition la culture de la case est modifiée*/
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  void settabRavageur(vector<int>  const &tab);
  /*Post-condition le tableau de ravageur de la case est modifié*/
||||||| parent of 461ce8c (reformat)
  void settabRavageur(vector<int> tab);
  /*Post-condition le tableau de ravageur de la case est modifié*/
=======
    Culture getcult() const;
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  void settabProtect(vector<int> const &tab);
  /*Post-condition le tableau de protecteur de la case est modifié*/
||||||| parent of 461ce8c (reformat)
  void settabProtect(vector<int> tab);
  /*Post-condition le tableau de protecteur de la case est modifié*/
=======
    /*Post-condition retourne la culture de la case*/
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  [[nodiscard]] vector<int> gettabProtect() const;
  /*Post-condition retourne le tableau de protecteur de la case*/
||||||| parent of 461ce8c (reformat)
  vector<int> gettabProtect() const;
  /*Post-condition retourne le tableau de protecteur de la case*/
=======
    void setcult(Culture c);
>>>>>>> 461ce8c (reformat)

<<<<<<< HEAD
  [[nodiscard]] vector<int> gettabRavageur() const;
  /*Post-condition retourne le tableau de ravageur de la case*/
||||||| parent of 461ce8c (reformat)
  vector<int> gettabRavageur() const;
  /*Post-condition retourne le tableau de ravageur de la case*/
=======
    /*Post-condition la culture de la case est modifiée*/
>>>>>>> 461ce8c (reformat)

    int getvaleur() const;

    /*Pré-condition un champs est présent sur la case
      Post-condition retourne la valeur de la case*/

    void setvaleur(unsigned int val);

<<<<<<< HEAD
  int calculeValeur();
  /*Post-condition la valeur de la case est calculée*/
||||||| parent of 461ce8c (reformat)
  unsigned int calculeValeur();
  /*Post-condition la valeur de la case est calculée*/
=======
    void settabRavageur(vector<int> tab);

    /*Post-condition le tableau de ravageur de la case est modifié*/

    void settabProtect(vector<int> tab);

    /*Post-condition le tableau de protecteur de la case est modifié*/

    vector<int> gettabProtect() const;

    /*Post-condition retourne le tableau de protecteur de la case*/

    vector<int> gettabRavageur() const;

    /*Post-condition retourne le tableau de ravageur de la case*/

    void ajouteProtecteur(int p);

    /*Post-condition un protecteur a été ajouté au tableau*/

    void ajouteRavageur(int r);

    /*Post-condition un ravageur a été ajouté au tableau*/

    void afficheCase() const;

    /*Post-condition la case est affichée*/

    unsigned int calculeValeur();
    /*Post-condition la valeur de la case est calculée*/
>>>>>>> 461ce8c (reformat)
};

#endif
