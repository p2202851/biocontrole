#include "Menu.h"
#include <cassert>
#include <SFML/Graphics.hpp>
#include "imgui-master/imgui.h"
#include "Case.h"
#include "Protecteur.h"
#include "imgui-sfml-2.6.x/imgui-SFML.h"

Menu::Menu() : longueur(400), largeur(400), protectSet(false), galSet(false), optSet(false) {
    jeu = new Jeu();
    sf::RectangleShape BoutonDeb(sf::Vector2f(120.f, 50.f));
    if (!textureBGMenu.loadFromFile("../data/starting_screen_pref.jpg")) {
        std::cout << "Erreur de chargement de l'image" << std::endl;
    }
    spriteBGMenu.setTexture(textureBGMenu);
    spriteBGMenu.setPosition(0, 0);

    if (!textureBouton.loadFromFile("../data/bouton-start.png")) {
        std::cout << "Erreur de chargement de l'image" << std::endl;
    }
    spriteBoutonDeb.setTexture(textureBouton);
    spriteBoutonDeb.setScale(0.3, 0.3);
    spriteBoutonDeb.setPosition(  static_cast< float >(window.getSize().x) / 2 - 80,  static_cast< float >(window.getSize().y) / 2 - 25);

    if (!textureBoutonGal.loadFromFile("../data/bouton-galerie.png")) {
        std::cout << "Erreur de chargement de l'image" << std::endl;
    }
    spriteBoutonGal.setTexture(textureBoutonGal);
    spriteBoutonGal.setScale(0.3, 0.3);
    spriteBoutonGal.setPosition(static_cast< float >(window.getSize().x) / 2 - 75, static_cast< float >(window.getSize().y) / 2 + 100);

    if (!textureBoutonOpt.loadFromFile("../data/bouton-option.png")) {
        std::cout << "Erreur de chargement de l'image" << std::endl;
    }
    spriteBoutonOpt.setTexture(textureBoutonOpt);
    spriteBoutonOpt.setScale(0.3, 0.3);
    spriteBoutonOpt.setPosition(25, 25);

    for (int i = 0; i < 8; i++) {
        if (!textureProtect[i].loadFromFile("../data/Protect" + std::to_string(i + 1) + ".png")) {
            std::cout << "Erreur de chargement de l'image" << std::endl;
        }
        spriteProtect[i].setTexture(textureProtect[i]);
        spriteProtect[i].setScale(0.3, 0.3);
    }
    for (int i = 0; i < 5; i++)
    {
        if (!textureCulture[i].loadFromFile("../data/Culture" + std::to_string(i + 1) + ".png"))
        {
            std::cout << "Erreur de chargement de l'image" << std::endl;
        }
        spriteCulture[i].setTexture(textureCulture[i]);
        spriteCulture[i].setScale(0.3, 0.3);
    }
}

Menu::~Menu() {
    delete jeu;
}

bool Menu::getProtectSet() {
    return protectSet;
}

void Menu::afficherMenu() {
    window.draw(spriteBGMenu);
    window.draw(spriteBoutonDeb);
    window.draw(spriteBoutonGal);
    window.draw(spriteBoutonOpt);
    window.draw(spriteBoutonBoutique);
}

void Menu::afficheBoutiqueP() {
    ImGui::Begin("Boutique des protecteurs");
    ImGui::Text("Bienvenue dans la boutique des protecteurs");
    for (int i = 0; i < 8; i++) {
        ImGui::Image(reinterpret_cast<ImTextureID>(textureProtect[i].getNativeHandle()), ImVec2(100, 100));
        if (ImGui::Button("acheter")) {
            std::cout << "Achat du protecteur " << i + 1 << std::endl;
            jeu->achatProtecteur(i + 1);
            jeu->afficheJeu();
        }
    }
    ImGui::End();
}

void Menu::afficheBoutiqueC() const {
    ImGui::SetNextWindowSize(ImVec2(600, 280), ImGuiCond_Once); // Set the window size
    //ImGui::SetNextWindowPos(ImVec2(0, 400), ImGuiCond_Once); // Set the window position
    ImGui::Begin("Boutique des cultures");
    for (int i = 0; i < 5 ; i++) {
        if (i < 3) {
            ImGui::SetCursorPos(ImVec2(25 * i * 8 + 20, 25));
        } else {
            ImGui::SetCursorPos(ImVec2(25 * (i - 3) * 8 + 20, 150));
        }
        ImGui::Image(reinterpret_cast<ImTextureID>(textureCulture[i].getNativeHandle()), ImVec2(100, 100));
        if (i < 3) {
            ImGui::SetCursorPos(ImVec2(25*i*8, 125)); // Move the cursor down for the button
        } else {
            ImGui::SetCursorPos(ImVec2(25*(i-3)*8, 250)); // Move the cursor down for the button
        }
        if (ImGui::Button(("Acheter Culture " + std::to_string(i+1)).c_str())) // Unique label for each button
        {
            std::cout << "Achat de la culture " << i + 1 << std::endl;
            //jeu->achatCulture(i+1);
            jeu->afficheJeu();
        }
    }

    ImGui::End();
}

void Menu::afficheDeck() const {
    ImGui::SetNextWindowSize(ImVec2(300, 200), ImGuiCond_Once); // Set the window size
    ImGui::SetNextWindowPos(ImVec2(200, 900), ImGuiCond_Once); // Set the window position
    ImGui::Begin("Deck", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar| ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoBackground);
    Protecteur deck[10];
    jeu->getDeckjJoueur(deck);
    int deplacement = 0;
    for (Protecteur const &i : deck) {
        if (i.getid() != 0) {
            ImGui::SetCursorPos(ImVec2(25+deplacement, 0)); // Move the cursor down for the button
            ImGui::Text( i.getnom().c_str());
            ImGui::SetCursorPos(ImVec2(25+deplacement, 20)); // Move the cursor down for the button
            ImGui::Image(reinterpret_cast<ImTextureID>(textureProtect[i.getid() - 1].getNativeHandle()), ImVec2(100, 100));
            deplacement += 150;
        }
    }
    ImGui::End();
}

void Menu::afficheInfoCase(int const x, int const y) const {
    //affichage du text
    ImGui::SetNextWindowSize(ImVec2(300, 200), ImGuiCond_Once); // Set the window size
    ImGui::SetNextWindowPos(ImVec2(0, 400), ImGuiCond_Once); // Set the window position
    Case const &c = jeu->getPlateau().getTabCaseIndice(x, y);
    ImGui::Begin("Information de la case");
    ImGui::Text("Case (%d, %d)", x, y);
    ImGui::Text("Valeur de la case : %d", c.getvaleur());
    ImGui::Text("Protecteur : ");
    for (int i: c.gettabProtect()) {
        if (i != 0) {
            ImGui::Text("Protecteur %d", i);
        }
    }
    ImGui::Text("Ravageur : ");
    for (int i: c.gettabRavageur()) {
        if (i != 0) {
            ImGui::Text("Ravageur %d", i);
        }
    }

    // Récupération du deck du joueur
    Protecteur deck[10];
    //Protecteur p(2);
    //Protecteur p2(3);
    jeu->getDeckjJoueur(deck);
    /*if (deck[0].getid() == 0) {
        jeu->ajoutDeckjJoueur(p);
        jeu->ajoutDeckjJoueur(p2);
    }*/

// Initialisation de items avec les noms des protecteurs de deckJoueur
    std::vector<string> items;
    items.clear();
    for (const auto& protecteur : deck) {
        if (protecteur.getid() != 0) { // Si le protecteur est valide (id différent de 0)
            items.push_back(protecteur.getnom()); // Ajoutez le nom du protecteur à items
        }
    }

// Affichage du menu déroulant avec les noms des protecteurs
    static string current_item = "Choisir un protecteur" ;
    if (ImGui::BeginCombo("##combo", current_item.c_str()))
    {
        for (auto & item : items)
        {
            bool is_selected = (current_item == item);
            if (ImGui::Selectable(item.c_str(), is_selected))
                current_item = item;
            if (is_selected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    if (ImGui::Button("Placer"))
    {
        cout << "Bouton cliqué" << endl;
        if (current_item != "Choisir un protecteur" ) {
            for (const Protecteur& i: deck) {
                if (i.getnom() == current_item) {
                    cout << "Utilisation du protecteur " << i.getid() << endl;
                    jeu->utiliserProtecteur(i.getid(), x, y);
                    jeu->getPlateau().getTabCaseIndice(x, y).afficheCase();
                    break;
                }
            }
        }

    }
    ImGui::End();
}