#include "Protecteur.h"
#include <iostream>

using namespace std;

int main() {
    Protecteur p(6);
    unsigned int id, de, efficacite;
    int nb_cible;
    string image, nom;
    vector<int> cible;
    bool propage, reste;
    id = p.getid();
    de = p.getde();
    efficacite = p.getefficacite();
    nb_cible = p.getnb_cible();
    image = p.getim();
    cout << "id: " << id << endl;
    cout << "de: " << de << endl;
    cout << "efficacite: " << efficacite << endl;
    cout << "nb_cible: " << nb_cible << endl;
    for (int i = 0; i < nb_cible; i++) {
        cout << "cible: " << p.getcible(i) << endl;
    }
    cout << "image: " << image << endl;
    return 0;
}