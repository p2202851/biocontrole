#include "affichagetxt.h"
#include <iostream>
#include "Ravageur.h"

using namespace std;

AffichageTxt::AffichageTxt() {
    jeu = new Jeu();
}

AffichageTxt::~AffichageTxt() {
    delete jeu;
}

void AffichageTxt::afficherInfoJeu() const {
    cout << "======== Info Jeu ========" << endl;
    jeu->afficheJeu();
    cout << "==========================" << endl;
}

Jeu *AffichageTxt::getJeu() const {
    return jeu;
}

void AffichageTxt::setJeu(Jeu *j) {
    delete jeu;
    jeu = j;
}

void AffichageTxt::afficherPlateau() const {
    cout << "======== Plateau ========" << endl;
    Plateau plateau;
    plateau = jeu->getPlateau();
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 5; j++) {
            Case c = plateau.getTabCaseIndice(i, j);
            if (c.gettabRavageur().size() > 0) {
                cout << "R";
            } else {
                if (c.getcult().getnbpoint() > 0) {
                    cout << "C";
                } else {
                    cout << "*";
                }
            }
            cout << "   ";
        }
        cout << endl;
    }
    cout << "==========================" << endl;
}

void AffichageTxt::afficherDetails(int i, int j) const {
    Plateau plateau;
    plateau = jeu->getPlateau();
    Case c = plateau.getTabCaseIndice(i, j);
    cout << "======== Case " << i << " ========" << j << endl;
    c.afficheCase();
    cout << "==========================" << endl;
}

void AffichageTxt::afficheInfoRavageur(int i) const {
    Ravageur r(i);
    cout << "======== Ravageur " << i << " ========" << endl;
    r.afficheRavageur();
    cout << "==========================" << endl;
}
