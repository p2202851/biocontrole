#include "Case.h"

#include <iostream>

using namespace std;

int main() {
    Vect2D v(2, 5);
    Case c(v);
    Vect2D v2;
    c.rensCult(5);
    v2 = c.getVect2D();
    int valeur = c.getvaleur();
    vector<int> tabProtect = c.gettabProtect();
    vector<int> tabRavageur = c.gettabRavageur();
    Culture cult = c.getcult();
    cout << "Vecteur 2D : " << v2.getX() << " " << v2.getY() << endl;
    cout << "tabProtect :" << endl;
    for (int i = 0; i < tabProtect.size(); i++) {
        cout << tabProtect[i] << endl;
    }
    cout << "tabRavageur :" << endl;
    for (int i = 0; i < tabRavageur.size(); i++) {
        cout << tabRavageur[i] << endl;
    }
    cout << "valeur :" << valeur << endl;
    cult.afficherCult();
    cout << "====================================" << endl;
    Vect2D v3(8, 3);
    c.setVect2D(v3);
    Culture cult2(3);
    c.setcult(cult2);
    c.ajouteRavageur(5);
    v2 = c.getVect2D();
    valeur = c.getvaleur();
    tabProtect = c.gettabProtect();
    tabRavageur = c.gettabRavageur();
    cult = c.getcult();
    cout << "Vecteur 2D : " << v2.getX() << " " << v2.getY() << endl;
    cout << "tabProtect :" << endl;
    for (int i = 0; i < tabProtect.size(); i++) {
        cout << tabProtect[i] << endl;
    }
    cout << "tabRavageur :" << endl;
    for (int i = 0; i < tabRavageur.size(); i++) {
        cout << tabRavageur[i] << endl;
    }
    cout << "valeur :" << valeur << endl;
    cult.afficherCult();
    cout << "====================================" << endl;
    c.ajouteProtecteur(1);
    c.ajouteProtecteur(5);
    v2 = c.getVect2D();
    valeur = c.getvaleur();
    tabProtect = c.gettabProtect();
    tabRavageur = c.gettabRavageur();
    cult = c.getcult();
    cout << "Vecteur 2D : " << v2.getX() << " " << v2.getY() << endl;
    cout << "tabProtect :" << endl;
    for (int i = 0; i < tabProtect.size(); i++) {
        cout << tabProtect[i] << endl;
    }
    cout << "tabRavageur :" << endl;
    for (int i = 0; i < tabRavageur.size(); i++) {
        cout << tabRavageur[i] << endl;
    }
    cout << "valeur :" << valeur << endl;
    cult.afficherCult();
    return 0;
}