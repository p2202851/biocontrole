#include "Vect2D.h"
#include <cassert>

Vect2D::Vect2D() : x(0.0), y(0.0) {}

Vect2D::Vect2D(double x_val, double y_val) : x(x_val), y(y_val) {}

Vect2D Vect2D::operator+(const Vect2D &other) const {
    return Vect2D(x + other.x, y + other.y);
}

Vect2D Vect2D::operator-(const Vect2D &other) const {
    return Vect2D(x - other.x, y - other.y);
}

Vect2D Vect2D::operator*(double scalar) const {
    return Vect2D(x * scalar, y * scalar);
}

Vect2D Vect2D::operator/(double scalar) const {

    assert(scalar != 0.0);
    return {x / scalar, y / scalar};
}

bool Vect2D::operator==(const Vect2D &other) const {
    return x == other.x && y == other.y;
}

// Getter pour la composante x
double Vect2D::getX() const {
    return x;
}

// Getter pour la composante y
double Vect2D::getY() const {
    return y;
}

// Setter pour la composante x
void Vect2D::setX(double x_val) {
    x = x_val;
}

// Setter pour la composante y
void Vect2D::setY(double y_val) {
    y = y_val;
}

// Affichage surchargé
ostream &operator<<(ostream &os, const Vect2D &vecteur) {
    os << "(" << vecteur.getX() << ", " << vecteur.getY() << ")";
    return os;
}
