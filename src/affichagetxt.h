#ifndef AFFICHAGETXT
#define AFFICHAGETXT

#include "Jeu.h"

class AffichageTxt {
private:
    Jeu *jeu;

public:
    AffichageTxt();

    /*Post-condition l'affichage est initialisé avec le jeu j*/

    ~AffichageTxt();

    /*Post-condition l'affichage est détruit*/

    [[nodiscard]] Jeu *getJeu() const;

    /*Post-condition retourne le jeu*/

    void setJeu(Jeu *j);

    /*Post-condition le jeu est modifié*/

    void
    afficherPlateau() const;

    /*Post-condition l'affichage est modifié*/

    void afficherInfoJeu() const;

    /*Post-condition l'affichage est modifié*/

    void afficherDetails(int i, int j) const;

    /*Post-condition l'affichage est modifié*/

    static void afficheInfoRavageur(int i) ;
    /*Post-condition l'affichage est modifié*/
};

#endif