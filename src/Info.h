#ifndef _INFO_H
#define _INFO_H

#include <string>

using namespace std;

class Info {
private:
    unsigned int id;
    string image, description, nom, type;
public:
    Info(); // Constructeur de la classe
    /*Post-condition l'info est initialisé avec les valeurs passée en parametres*/

    void InfoCult(int id); // Constructeur de la classe
    /*Post-condition l'info est initialisé avec les valeurs passée en parametres*/

    void InfoRav(int id); // Constructeur de la classe
    /*Post-condition l'info est initialisé avec les valeurs passée en parametres*/

    void InfoProt(int id); // Constructeur de la classe
    /*Post-condition l'info est initialisé avec les valeurs passée en parametres*/

    ~Info(); // Destructeur de la classe
    /*Post-condition l'info est détruite*/

    unsigned int getid() const; // Retourne l'id de l'info

    string getimage() const; // Retourne l'image de l'info

    string getdescription() const; // Retourne la description de l'info

    string getnom() const; // Retourne le nom de l'info

    string gettype() const; // Retourne le type de l'info

};

#endif 