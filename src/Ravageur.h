#ifndef RAVAGEUR
#define RAVAGEUR

#include <vector>
#include <string>

using namespace std;

class Case;

class Ravageur {
private:
    unsigned int id;
    string image, nom;
    int nb_cible, nb_menace, pourcentage;
    vector<int> cible;
    vector<int> menace;
    bool propage;

public:
    Ravageur(int id_nouv);

    /*Post-condition le ravageur est initialisé avec les valeurs presentes dans le fichier*/

    ~Ravageur();

    /*Post-condition le ravageur est détruit*/

    int getid() const;

    /*Post-condition retourne l'id du ravageur*/

    string getim() const;

    /*Post-condition retourne l'image du ravageur*/

    int getnb_cible() const;

    /*Post-condition retourne le nombre de cibles du ravageur*/

    int getcible(int i) const;

    /*Post-condition retourne la cible i du ravageur*/

    int getnb_menace() const;

    /*Post-condition retourne le nombre de menaces du ravageur*/

    int getpourcentage() const;

    /*Post-condition retourne le pourcentage de menace du ravageur*/

    int getmenace(int i) const;

    /*Post-condition retourne la menace i du ravageur*/

    void afficheRavageur() const;

    /*Post-condition affiche les informations du ravageur*/

    bool cibleEstPresente(const Case &c) const;

    /*Pré-condition le ravageur se trouve sur la case c
      Post-condition retourne vrai si la cible est présente sur la case et faux sinon*/

    bool protectPresent(const Case &c) const;
    /*Pré-condition le ravageur se trouve sur la case c
      Post-condition retourne vrai si un protecteur est présent sur la case et faux sinon*/
};

#endif
