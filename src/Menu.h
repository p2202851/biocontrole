#ifndef MENU_H
#define MENU_H

#include "Jeu.h"
#include <SFML/Graphics.hpp>
#include <imgui.h>
#include <imgui-SFML.h>

class Menu {
private:
    Jeu *jeu;
    int longueur;
    int largeur;
    bool  galSet, optSet;
    sf::RectangleShape BoutonDeb;
    sf::Sprite spriteCulture[5];
    sf::Texture textureCulture[5];
    sf::Sprite spriteProtect[8];
    sf::Texture textureProtect[8];


public:
    sf::RenderWindow window;
    sf::Sprite spriteBoutonBoutique;
    sf::Sprite spriteBGMenu;
    sf::Sprite spriteBoutonDeb;
    sf::Sprite spriteBoutonGal;
    sf::Sprite spriteBoutonOpt;
    sf::Texture textureBoutique;
    sf::Texture textureBGMenu;
    sf::Texture textureBouton;
    sf::Texture textureBoutonGal;
    sf::Texture textureBoutonOpt;

    Menu();

    ~Menu();

    void afficherMenu(); // Affiche le menu

    void afficherOption(); // Affiche les options

    void afficherGal(); // Affiche les choix de galerie possibles

    void start(); // Lance le jeu

    void afficheBoutiqueP() const; // Affiche la boutique de protecteur

    void afficheBoutiqueC() const; // Affiche la boutique de culture

    void afficheInfoCase(int const x, int const y) const; // Affiche les informations de la case

    void afficheDeck() const; // Affiche le deck
};

#endif