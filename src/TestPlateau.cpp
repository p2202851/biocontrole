#include "Plateau.h"

int main() {
    Plateau p;

    Vect2D v(40, 30);
    Vect2D v2(80, 20);
    Case c(v);
    Case c2(v2);
    c.rensCult(1);
    c2.rensCult(2);

    // Teste de la fonction setTabCase
    p.setTabCase(c);
    p.setTabCase(c2);

    // Teste de la fonction getTabCaseVect et getTabCaseIndice
    cout << "======== Case 1 ========" << endl;
    c2 = p.getTabCaseVect(v);
    c2.afficheCase();
    cout << "======== Case 2 ========" << endl;
    c2 = p.getTabCaseIndice(4, 2);
    c2.afficheCase();

    // Teste de la fonction calculeValeur
    cout << "======== Valeur du plateau ========" << endl;
    cout << p.calculeValeur() << endl;

    return 0;
}