#ifndef PLATEAU
#define PLATEAU

#include "Case.h"

class Plateau {
private:
    int longueur = 6;
    int largeur = 5;
    Case tabCase[6][5];

public:
    Plateau();

    /*Post-condition le plateau est initialisé avec les valeurs presentes dans le fichier*/

    ~Plateau();

    /*Post-condition le plateau est détruit*/

    Case getTabCaseVect(Vect2D vect) const;

    /*Post-condition retourne la case i du plateau*/

    Case getTabCaseIndice(int i, int j) const;

    void setTabCase(Case const &c);
    /*Post-condition la case c est ajoutée au plateau*/

    void affichePlateau() const;

    /*Post-condition le plateau est affiché*/

    [[nodiscard]] int calculeValeur() const;
    /*Post-condition la valeur de chaque case est calculée*/
};

#endif
