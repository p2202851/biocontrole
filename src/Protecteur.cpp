#include "Protecteur.h"
#include "Case.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

Protecteur::Protecteur(int id_nouv) {
    ifstream Caract("../data/Protecteur.txt");
    if (Caract.is_open()) {
        string tampon;
        Caract >> id;
        while (id_nouv != id) {
            getline(Caract, tampon);
            Caract >> id;
        }
        Caract >> nom;
        Caract >> de;
        Caract >> efficacite;
        Caract >> image;
        Caract >> nb_cible;
        for (int i = 0; i < nb_cible; i++) {
            int tampon;
            Caract >> tampon;
            cible.push_back(tampon);
        }
        Caract >> prix;

        Caract >> tampon;
        if (tampon == "true") {
            propage = true;
        } else {
            propage = false;
        }
        Caract >> tampon;
        if (tampon == "true") {
            reste = true;
        } else {
            reste = false;
        }
    } else {
        cout << "Erreur à l'ouverture" << endl;
    }
    Caract.close();
}

Protecteur::~Protecteur() {
}

int Protecteur::getid() const {
    return id;
}

int Protecteur::getde() const {
    return de;
}

int Protecteur::getefficacite() const {
    return efficacite;
}

int Protecteur::getprix() const {
    return prix;
}

string Protecteur::getim() const {
    return image;
}

bool Protecteur::getreste() const {
    return reste;
}

int Protecteur::getnb_cible() const {
    return nb_cible;
}

int Protecteur::getcible(int i) const {
    return cible[i];
}

string Protecteur::getnom() const {
    return nom;
}

bool Protecteur::cibleEstPresente(const Case &c) const {
    int i, j;
    vector<int> tabRavageur;
    c.gettabRavageur();
    for (i = 0; i < cible.size(); i++) {
        for (j = 0; j < tabRavageur.size(); j++) {
            if (cible[i] == tabRavageur[j]) {
                return true;
            }
        }
    }
    return false;
}

void Protecteur::fonctionne(Case c) {
    int compt;
    vector<int> tabRavageur;
    c.gettabRavageur();
    if (cibleEstPresente(c)) {
        vector<int> nouv_pres;
        for (int i = 0; i < tabRavageur.size(); i++) {
            compt = 5;
            for (int j = 0; j < cible.size(); j++) {
                if (cible[j] == tabRavageur[i]) {
                    compt--;
                }
            }
            if (compt == 5) {
                nouv_pres.push_back(tabRavageur[i]);
            }
        }
        c.settabRavageur(nouv_pres);
    }
}

void Protecteur::afficheProtecteur() const {
    cout << "id: " << id << endl;
    cout << "nom: " << nom << endl;
    cout << "de: " << de << endl;
    cout << "efficacite: " << efficacite << endl;
    cout << "nb_cible: " << nb_cible << endl;
    for (int i = 0; i < nb_cible; i++) {
        cout << "cible " << i << ": " << cible[i] << endl;
    }
    cout << "propage: " << propage << endl;
    cout << "reste: " << reste << endl;
}