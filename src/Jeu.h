#ifndef JEU
#define JEU

#include "Protecteur.h"
#include "Culture.h"
#include "Plateau.h"

class Jeu {
private:
    Plateau plateau;
    unsigned int round;
    int argent;
    Protecteur shopProtect[13];
    Culture shopCulture[5];
    Protecteur decjJoueur[10];

public:
    Jeu();

    /*Post-condition le jeu est initialisé avec les valeurs presentes dans le fichier*/

    ~Jeu();

    /*Post-condition le jeu est détruit*/

    void afficheJeu() const;

    /*Post-condition affiche le jeu*/

    unsigned int getround() const;

    /*Post-condition retourne le round actuel*/

    int getargent() const;

    /*Post-condition retourne l'argent actuel*/

    void getShopProtect(Protecteur shop[13]) const;

    /*Post-condition retourne la boutique de protecteurs*/

    void getShopCulture(Culture shop[5]) const;

    /*Post-condition retourne la boutique de cultures*/

    Plateau getPlateau() const;

    /*Post-condition retourne le plateau*/

    void plusRound();

    /*Post-condition le round est modifié*/

    void modifArgent(unsigned int a);

    /*Post-condition l'argent est modifié*/

    void ajoutDeckjJoueur(Protecteur p);

    /*Post-condition un protecteur est ajouté au deck*/

    Protecteur retireDeckjJoueur(int i);

    /*Post-condition le protecteur i est retiré du deck*/

    void utiliserProtecteur(int i, int x, int y);

    /*Post-condition le protecteur d'indice i du deck est utilisé*/

    void achatProtecteur(int p);

    /*Post-condition le protecteur est acheté*/

    void achatCulture(int c, int x, int y);

    /*Post-condition la culture est achetée*/

    void finround();

    /*Post-condition le round est terminé*/

    void finjeu();
    /*Post-condition le jeu est terminé*/
};

#endif
