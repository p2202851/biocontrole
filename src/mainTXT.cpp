#include "affichagetxt.h"

#include <iostream>
#include <string>

using namespace std;

int main() {
    AffichageTxt a;
    while (a.getJeu()->getround() < 8) {
        a.afficherInfoJeu();
        a.afficherPlateau();
        bool choix = false;
        while (!choix) {
            cout << "Voulez-vous avoir les informations d'une case ? (1: oui, 0: non)" << endl;
            int c;
            cin >> c;
            if (c == 1) {
                cout << "Quel case voulez-vous inspecter ? (indice x puis y)" << endl;
                int x, y;
                cin >> x;
                cin >> y;
                a.afficherDetails(x, y);
            } else if (c == 0) {
                choix = true;
            }
        }
        choix = false;
        while (!choix) {
            cout << "Voulez-vous avoir les informations d'un ravageur ? (1: oui, 0: non)" << endl;
            int c;
            cin >> c;
            if (c == 1) {
                cout << "Quel ravageur voulez-vous inspecter ? (numéro)" << endl;
                int x;
                cin >> x;
                a.afficheInfoRavageur(x);
            } else if (c == 0) {
                choix = true;
            }
        }
        choix = false;
        while (!choix) {
            int c;
            cout << "Voulez-vous acheter un protecteur ? (1: oui, 0: non)" << endl;
            cin >> c;
            if (c == 1) {
                cout << "Quel protecteur voulez-vous acheter ? (numéro)" << endl;
                int x;
                cin >> x;
                a.getJeu()->achatProtecteur(x);
            } else {
                choix = true;
            }
        }
        choix = false;
        while (!choix) {
            cout << "Voulez-vous acheter une culture ? (1: oui, 0: non)" << endl;
            int c;
            cin >> c;
            if (c == 1) {
                cout << "Quel culture voulez-vous acheter ? (numéro)" << endl;
                int x;
                cin >> x;
                cout << "Où voulez-vous la placer ? (indice x puis y)" << endl;
                int y, z;
                cin >> y;
                cin >> z;
                a.getJeu()->achatCulture(x, y, z);
            } else {
                choix = true;
            }
        }
        choix = false;
        while (!choix) {
            int c;
            cout << "Voulez-vous utiliser un protecteur ? (1: oui, 0: non)" << endl;
            cin >> c;
            if (c == 1) {
                cout << "Quel protecteur voulez-vous utiliser ? (numéro)" << endl;
                int x;
                cin >> x;
                cout << "Où voulez-vous le placer ? (indice x puis y)" << endl;
                int y, z;
                cin >> y;
                cin >> z;
                a.getJeu()->utiliserProtecteur(x, y, z);
            } else {
                choix = true;
            }
        }
        choix = false;
        int c;
        cout << "Voulez-vous passez au round suivant ? (1: oui, 0: non)" << endl;
        cin >> c;
        if (c == 1) {
            a.getJeu()->finround();
        }
    }
    return 0;
}