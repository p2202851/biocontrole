{
  inputs = {
    nixpkgs.url = "nixpkgs";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-parts,
    rust-overlay,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];

      perSystem = {
        lib,
        system,
        ...
      }: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [(import rust-overlay)];
        };
      in {
        devShells.default = with pkgs;
          pkgs.mkShell {
            buildInputs = [
                #common
                gitlab-runner
                #pkgs.clang-tools
                #llvm.clang # clangd
                clang
                #clang-tools
                cmake
                ninja
                gcc
                valgrind
                doxygen
                graphviz
                llvm
                sfml
                libGLU
            ];
LD_LIBRARY_PATH="/run/opengl-driver/lib:/run/opengl-driver-32/lib";
#            LD_LIBRARY_PATH = lib.makeLibraryPath [
##              vulkan-loader
#              libxkbcommon
#              opengl
#              wayland
#            ];
          };

        formatter = pkgs.alejandra;
      };
    };
}
