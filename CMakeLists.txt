# Version minimale de CMake requise
cmake_minimum_required(VERSION 3.10)

# Nom du projet
project(Biocontrole)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_BUILD_TYPE Debug)

set(SFML_DIR "</lib/x86_64-linux-gnu/cmake/SFML")
find_package(SFML 2.5 COMPONENTS graphics audio window system REQUIRED)



set(SOURCES
    ../src/Protecteur.cpp
    ../src/Case.cpp
    ../src/Vect2D.cpp
    ../src/Ravageur.cpp
    ../src/Jeu.cpp
    ../src/Culture.cpp
    ../src/Plateau.cpp
    ../src/Info.cpp
    ../src/Galerie.cpp
    ../src/affichagetxt.cpp
    ../src/Menu.cpp
)

include(FetchContent)

# SFML
FetchContent_Declare(
        sfml
        URL https://github.com/SFML/SFML/archive/refs/tags/2.5.1.zip
        URL_MD5 2c4438b3e5b2d81a6e626ecf72bf75be
)

# Dear ImGui
FetchContent_Declare(
        imgui
        GIT_REPOSITORY https://github.com/ocornut/imgui
        GIT_TAG 35b1148efb839381b84de9290d9caf0b66ad7d03 # 1.82
)

FetchContent_MakeAvailable(imgui)

# ImGui-SFML
FetchContent_Declare(
        imgui-sfml
        GIT_REPOSITORY https://github.com/SFML/imgui-sfml
        GIT_TAG 82dc2033e51b8323857c3ae1cf1f458b3a933c35 # 2.3
)
message(STATUS "Fetching ImGui-SFML...")

set(IMGUI_DIR ${imgui_SOURCE_DIR})
set(IMGUI_SFML_FIND_SFML OFF)
set(IMGUI_SFML_IMGUI_DEMO ON)

FetchContent_MakeAvailable(imgui-sfml)

#add_executable(TestRav ${SOURCES} ../src/TestRav.cpp)
#add_executable(TestProt ${SOURCES} ../src/TestProt.cpp)
#add_executable(TestCult ${SOURCES} ../src/TestCult.cpp)
#add_executable(TestCase ${SOURCES} ../src/TestCase.cpp)
#add_executable(TestPlat ${SOURCES} ../src/TestPlateau.cpp)
#add_executable(TestJeu ${SOURCES} ../src/TestJeu.cpp)
#add_executable(TestAfficheTXT ${SOURCES} ../src/testAfficheTxt.cpp)

if(POLICY CMP0072)
  cmake_policy(SET CMP0072 NEW)
endif()

find_package(OpenGL REQUIRED)

add_executable(Biocontrole ${SOURCES} ${IMGUI} ../src/mainTXT.cpp)
target_link_libraries(Biocontrole sfml-graphics sfml-audio sfml-window sfml-system OpenGL::GL)

add_executable(Biocontrole2 ${SOURCES} ${IMGUI} ../src/affichageSFML.cpp)

target_include_directories(Biocontrole2 PRIVATE
        "${IMGUI_INCLUDE_DIR}"
)

target_include_directories(Biocontrole2 PRIVATE
        "${IMGUI_SFML_INCLUDE_DIR}"
)

target_link_libraries(Biocontrole2 sfml-graphics sfml-audio sfml-window sfml-system OpenGL::GL)

#add_executable(TestJeu ${SOURCES} ${IMGUI} ../src/TestJeu.cpp)
#target_link_libraries(TestJeu sfml-graphics sfml-audio sfml-window sfml-system OpenGL::GL)
